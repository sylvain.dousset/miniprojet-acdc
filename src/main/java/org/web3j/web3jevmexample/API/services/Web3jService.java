/*
 * Copyright 2019 Web3 Labs Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
 * an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations under the License.
 */
  
package org.web3j.api.services;

import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;

import static org.joor.Reflect.*;
import org.hyperledger.besu.ethereum.vm.OperationTracer;

import com.google.gson.JsonParser;
import com.google.gson.JsonObject;

import org.web3j.abi.datatypes.Address;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.evm.Configuration;
import org.web3j.evm.PassthroughTracer;
import org.web3j.evm.EmbeddedWeb3jService;
import org.web3j.protocol.Web3j;
import org.web3j.tx.gas.DefaultGasProvider;

import org.web3j.api.controllers.SmartContractController;
import org.web3j.api.services.Web3jService;
import org.web3j.generators.*;

public class Web3jService {

    private Credentials credentials;
    private Configuration configuration;
    private OperationTracer operationTracer;
    private Web3j web3j;
    private Class c;

    public Web3jService(String scriptName) throws Exception {
        this.c = SmartContractController.getScriptClass(scriptName);
        this.credentials = WalletUtils.loadCredentials("Password123", "resources/demo-wallet.json");
        this.configuration = new Configuration(new Address(credentials.getAddress()), 10);
        this.operationTracer = new PassthroughTracer();
    }

    private void buildWeb3j(){
        this.web3j = Web3j.build(new EmbeddedWeb3jService(this.configuration, this.operationTracer));
    }

    private String deployContract(){
        System.out.println("Deploying contract..");
        String deployedContractAddress = onClass(this.c)
            .call("deploy", this.web3j, this.credentials, new DefaultGasProvider())
            .call("send")
            .call("getContractAddress")
            .get();

        System.out.println("contract address: " + deployedContractAddress);
        return deployedContractAddress;
    }

    public int[][] evaluate(String scriptName, String params) throws Exception{
        JsonObject objet = new JsonParser().parse(params).getAsJsonObject();
        String inputType = objet.get("inputType").getAsString();
        int min = 0;
        int max = 0;
        int step = 0;
        try {
            min = Integer.parseInt(objet.get("min").getAsString());
            max = Integer.parseInt(objet.get("max").getAsString());
            step = Integer.parseInt(objet.get("step").getAsString());
        } 
        catch(NumberFormatException e) {
            System.out.println(e.getMessage());
        }
           
        System.out.println("Building web3j instance..");
        this.buildWeb3j();

        String deployedContractAddress = this.deployContract();
        
        if(inputType.equalsIgnoreCase("list-biginteger")){
            return this.evaluateAsListOfBigInteger(deployedContractAddress, min, max, step);
        }
        else if(inputType.equalsIgnoreCase("integer")){
            return this.evaluateAsInteger(deployedContractAddress, min, max, step);
        }
        else {
            return null;
        }
    }

    private int[][] evaluateAsListOfBigInteger(String deployedContractAddress, int minListSize, int maxListSize, int step){
        int numberOfResults = (int) Math.floor((maxListSize-minListSize) / step);
        int numberOfInput = minListSize;
        int[][] res = new int[numberOfResults+1][2];
        for(int i = 0 ; i <= numberOfResults ; i++){
            res[i][0] = numberOfInput;
            BigInteger bI = onClass(this.c)
                .call("load", deployedContractAddress, this.web3j, this.credentials, new DefaultGasProvider())
                .call("run", ListGenerator.generateRandomBigIntegerList(numberOfInput))
                .call("send")
                .call("getGasUsed")
                .get();
            res[i][1] = bI.intValue();
            numberOfInput = numberOfInput + step;
        }
        return res;
    }

    private int[][] evaluateAsInteger(String deployedContractAddress, int _min, int _max, int _step){
        int numberOfResults = (int) Math.floor((_max-_min) / _step);
        BigInteger intputNumber = new BigInteger(Integer.toString(_min));
        BigInteger step = new BigInteger(Integer.toString(_step));
        int[][] res = new int[numberOfResults+1][2];
        for(int i = 0 ; i <= numberOfResults ; i++){
            res[i][0] = intputNumber.intValue();
            BigInteger bI = onClass(this.c)
                .call("load", deployedContractAddress, this.web3j, this.credentials, new DefaultGasProvider())
                .call("run", intputNumber)
                .call("send")
                .call("getGasUsed")
                .get();
            res[i][1] = bI.intValue();
            intputNumber = intputNumber.add(step);
        }
        return res;
    }

}