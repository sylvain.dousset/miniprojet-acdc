package org.web3j.api.controllers;

import io.javalin.http.Context;
import io.javalin.plugin.openapi.annotations.*;

import java.io.IOException;
import java.net.URISyntaxException;

import org.web3j.api.doc_models.RunBody;
import org.web3j.api.controllers.FileController;
import org.web3j.api.controllers.SmartContractController;
import org.web3j.api.controllers.Web3jController;

public class ScriptsController {

    @OpenApi(
        path = "/scripts",            // only necessary to include when using static method references
        method = HttpMethod.GET,    // only necessary to include when using static method references
        summary = "Get all available scripts",
        operationId = "getAllScripts",
        tags = {"Scripts"},
        responses = {
            @OpenApiResponse(status = "200", content = {@OpenApiContent(from = String[].class)})
        }
    )
    public static void getAll(Context ctx) throws URISyntaxException{
        ctx.json(FileController.getAvailableScripts());
    }


    @OpenApi(
        path = "/scripts/:name",            // only necessary to include when using static method references
        method = HttpMethod.GET,    // only necessary to include when using static method references
        summary = "Get parameters types possibilities for a given script",
        operationId = "getScriptInformations",
        tags = {"Scripts"},
        responses = {
            @OpenApiResponse(status = "200", content = {@OpenApiContent(from = String[].class)})
        }
    )
    public static void getScriptInformations(Context ctx) throws ClassNotFoundException, IOException{
        String scriptName = ctx.pathParam("name");
        ctx.json(SmartContractController.listParametersOfMethod(scriptName));
    }

    @OpenApi(
        path = "/runScript/:name",            // only necessary to include when using static method references
        method = HttpMethod.POST,    // only necessary to include when using static method references
        summary = "Run script with given parameters",
        operationId = "runScript",
        tags = {"Scripts"},
        requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = RunBody[].class)}),
        responses = {
            @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Integer[].class)})
        }
    )
    public static void runScript(Context ctx) throws Exception{
        String scriptName = ctx.pathParam("name");
        int[][] res = Web3jController.evaluate(scriptName, ctx.body());
        if(res == null){
            ctx.result("Type not supported");
        }
        else{
            ctx.json(res);
        }
        
    }

    
}