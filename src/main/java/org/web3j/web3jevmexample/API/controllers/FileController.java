package org.web3j.api.controllers;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Objects;

public class FileController {

    public static String[] getAvailableScripts() throws URISyntaxException  {
        FilenameFilter filter = (dir, name) -> name.endsWith(".bin");
        URL ressouce = FileController.class.getClassLoader().getResource("solidity");
        File f = new File(ressouce.toURI());
        File[] files = f.listFiles(filter);
        String[] fileNames = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            fileNames[i] = files[i].getName().split("\\.")[0];
        }
        return fileNames;
    }

}