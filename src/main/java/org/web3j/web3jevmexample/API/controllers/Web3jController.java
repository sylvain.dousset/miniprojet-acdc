package org.web3j.api.controllers;

import org.web3j.api.services.Web3jService;

public class Web3jController {

    public static int[][] evaluate(String scriptName, String params) throws Exception {
        Web3jService service = new Web3jService(scriptName);
        return service.evaluate(scriptName, params);
    }

}