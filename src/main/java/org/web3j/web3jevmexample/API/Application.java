package org.web3j.api;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

import com.google.gson.Gson;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.web3j.api.controllers.ScriptsController;
import org.web3j.api.controllers.FileController;
import org.web3j.api.controllers.Web3jController;
import org.web3j.api.controllers.SmartContractController;

public class Application {

    public static void main(String[] args) {

        Javalin.create(config -> {
            config.registerPlugin(getConfiguredOpenApiPlugin());
            config.defaultContentType = "application/json";
        }).routes(() -> {
            path("scripts", () -> {
                get(ScriptsController::getAll);
                path(":name", () -> {
                    get(ScriptsController::getScriptInformations);
                });
            });

            path("runScript", () -> {
                path(":name", () -> {
                    post(ScriptsController::runScript);
                });
            });
        }).start(7000);

        System.out.println("Check out docs at http://localhost:7000/doc");
    }

    private static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").title("Smart Contracts API").description("API to interact with smart contracts");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("org.web3j.api")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/doc")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")); // endpoint for redoc
        return new OpenApiPlugin(options);
    }
    
    
}

