package org.web3j.generators;

import org.web3j.generators.IGenerator;
import java.util.Random;
import java.nio.charset.Charset;

public class StringGenerator implements IGenerator {

    public static String generateRandomString(){
        byte[] array = new byte[20]; // length is bounded by 20
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));
        return generatedString;
    }

}