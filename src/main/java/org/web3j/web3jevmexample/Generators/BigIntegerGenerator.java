package org.web3j.generators;

import org.web3j.generators.IGenerator;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class BigIntegerGenerator implements IGenerator {


    public static BigInteger generateRandomBigInteger(){
        BigInteger maxLimit = new BigInteger("1000");
        BigInteger minLimit = new BigInteger("1");
        BigInteger bigInteger = maxLimit.subtract(minLimit);
        Random randNum = new Random();
        int len = maxLimit.bitLength();
        BigInteger res = new BigInteger(len, randNum);
        if (res.compareTo(minLimit) < 0){
            res = res.add(minLimit);
        }
        if (res.compareTo(bigInteger) >= 0){
            res = res.mod(bigInteger).add(minLimit);
        }
        return res;
    }

}