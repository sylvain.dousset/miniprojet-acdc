package org.web3j.generators;

import org.web3j.generators.IGenerator;
import java.util.Random;

public class DoubleGenerator implements IGenerator {

    public static Double generateRandomDouble(){
        Double min = 1.0;
        Double max = 1000.0;
        Random r = new Random();
        double random = min + r.nextDouble() * (max - min);
        return  random;
    }

}