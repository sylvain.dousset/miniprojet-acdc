package org.web3j.generators;

import org.web3j.generators.IGenerator;
import org.web3j.generators.BigIntegerGenerator;
import org.web3j.generators.DoubleGenerator;
import org.web3j.generators.StringGenerator;
import java.math.BigInteger;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class ListGenerator implements IGenerator {

    public static List<BigInteger> generateRandomBigIntegerList(int listSize){
        List<BigInteger> list = new ArrayList<BigInteger>();
        for(int i = 0 ; i < listSize ; i++){
            list.add(BigIntegerGenerator.generateRandomBigInteger());
        }
        return list;
    }

    public static List<Double> generateRandomDoubleList(int listSize){
        List<Double> list = new ArrayList<Double>();
        for(int i = 0 ; i < listSize ; i++){
            list.add(DoubleGenerator.generateRandomDouble());
        }
        return list;
    }

    public static List<String> generateRandomStringList(int listSize){
        List<String> list = new ArrayList<String>();
        for(int i = 0 ; i < listSize ; i++){
            list.add(StringGenerator.generateRandomString());
        }
        return list;
    }

}