# Performance analysis on smart contracts
This project aims to compare the amount of gas used for the call of a smart contract methods based on different inputs

## Fonctionnalités

- Génération d'inputs et de listes aléatoires de différents types.
- Listage des scripts de smart contract disponible pour l'éxécution.
- Listage des paramétres possibles pour la méthode d'éxécution d'un smart contract.
- Exécution et récupération du coût en gas pour chaque éxécution d'un smart contract : les inputs sont paramétrables.

Après avoir lancer le projet, une documentation de l'API est disponnible sur localhost:7000/doc ou en bas de ce fichier.

## Lancer l'environnement de développement

`./gradlew clean build run`

## Environnement de développement

```
java -version :
java version "11.0.8" 2020-07-14 LTS
Java(TM) SE Runtime Environment 18.9 (build 11.0.8+10-LTS)
```

```
gradle -v:
Gradle 6.6.1
```

## Structure du projet

src/main/solidity: Les smart contracts en format .sol.

src/main/java/org/web3j/web3jevmexample:

  - /Genarators: Les generateurs d'inputs pour les smarts contracts.
  - /Application.java: La création des routes et du serveur Javalin.

  - /API:
  
    - /controllers: Les controlleurs qui intéragissent avec les smart contracts, fichiers et web3j.
    - /services/Web3jService: Le service utilisé pour éxécuter un smart contract.

### Attention

On fait l'hypothèse qu'un smart contract possède toujours une méthode `run` qui représente la méthode principale d'éxécution du script.

## Utilisation

`GET http://localhost:7000/scripts` : Liste les smarts contrats disponnibles

`GET http://localhost:7000/scripts/{scriptName}` : Liste les differents parametres disponnibles pour le smart contract dont le nom est passé en parametre.

`POST http://localhost:7000/runScript/{scriptName}` : Execute un le smart contract passé en parametre. 
Cette méthode attend un body de la forme : 
```
{
  "inputType": "list-bigInteger"
  "min": 10,
  "max": 20,
  "step": 10
}
```

Réponse sous la forme:
```
[
  [
    10,      //inputSize
    39148    //gasUsed
  ],
  [
    20,
    2222153
  ]
]
```

Les inputs type disponnibles sont : `["list-bigInteger", "list-double", "list-string", "integer", "string", "double"]`.

Attention ils sont disponible mais pas tous implémentés pour chaque smart contracts (e.g: on ne peut pas faire de fibonacci en donnant des chaines de caractères en entré).

